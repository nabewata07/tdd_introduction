require_relative 'bank'

class Money
  attr_reader :amount, :currency

  def initialize(amount, currency)
    @amount = amount
    @currency = currency
  end

  def times(multiplier)
    Money.new(@amount * multiplier, @currency)
  end

  def to_s
    "#{@amount} #{@currency}"
  end

  def self.dollar(amount)
    Money.new(amount, 'USD')
  end

  def self.franc(amount)
    Money.new(amount, 'CHF')
  end

  def equals?(money)
    return true if @amount == money.amount &&
      money.currency == @currency
    false
  end

  def plus(money)
    Money.new(money.amount + @amount, @currency)
  end

  protected :amount
end

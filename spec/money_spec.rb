require_relative '../money.rb'

describe Money do
  describe '#times' do
    context '5ドルを2倍すると' do
      it '２倍の額のDollarオブジェクトが作られる' do
        five = Money.dollar(5)
        expect(Money.dollar(10)).to be_equals(five.times(2))

        expect(Money.dollar(15)).to be_equals(five.times(3))
      end
    end
  end

  describe '#equals' do
    it '5ドルのオブジェクト同士は等しい' do
      expect(Money.dollar(5)).to be_equals(Money.dollar(5))
    end
    it '5ドルと6ドルは等しくない' do
      expect(Money.dollar(5)).not_to be_equals(Money.dollar(6))
    end
    it '5ドルと5フランは等しくない' do
      expect(Money.dollar(5)).not_to be_equals(Money.franc(5))
    end
  end

  describe '#currency' do
    it 'DollarのcurrencyはUSD' do
      expect(Money.dollar(1).currency).to eq('USD')
    end
    it 'FrancのcurrencyはCHF' do
      expect(Money.franc(1).currency).to eq('CHF')
    end
  end

  describe '#plus' do
    it '足し算した結果のExpressionオブジェクトを生成する' do
      five = Money.dollar(5)
      sum = five.plus(five)
      bank = Bank.new
      reduced = bank.reduce(sum, 'USD')
      expect(reduced).to be_equals(Money.dollar(10))
    end
  end
end
